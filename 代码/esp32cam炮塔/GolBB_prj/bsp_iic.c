#include "bsp_iic.h"
#include "esp_log.h"
 
void CARSlaveInit(void)
{
  static i2c_config_t conf;
  conf.mode = I2C_MODE_SLAVE;
  conf.sda_io_num = CAR_SDA_PIN;
  conf.sda_pullup_en = GPIO_PULLUP_ENABLE;
  conf.scl_io_num = CAR_SCL_PIN;
  conf.scl_pullup_en = GPIO_PULLUP_ENABLE;
  conf.slave.addr_10bit_en = 0;
  conf.slave.slave_addr = CAR_SLAVE_ADDR;
  
  i2c_param_config(I2C_SLAVE_NUM, &conf);
  i2c_driver_install(I2C_SLAVE_NUM, conf.mode, 128, 128, 0);//初始化
}

void CAR_SendData(uint8_t* arry, int len)
{
  i2c_reset_tx_fifo(I2C_SLAVE_NUM);
  i2c_slave_write_buffer(I2C_SLAVE_NUM, arry, len, AWAIT_BUF_TIME);
}
