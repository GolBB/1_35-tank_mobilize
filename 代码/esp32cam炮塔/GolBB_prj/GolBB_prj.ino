#include "esp_camera.h"
#include <Arduino.h>
#include <WiFi.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <iostream>
#include <sstream>
#include "bsp_iic.h"
#include "html_cpp.h"

//User data
const int PWMFreq = 20000000; /* 20 MHz */
const int PWMResolution = 8;
const int PWMSpeedChannel = 2;
const int PWMLightChannel = 3;
#define LIGHT_PIN 4
//Camera related constants
#define PWDN_GPIO_NUM     32
#define RESET_GPIO_NUM    -1
#define XCLK_GPIO_NUM      0
#define SIOD_GPIO_NUM     26
#define SIOC_GPIO_NUM     27
#define Y9_GPIO_NUM       35
#define Y8_GPIO_NUM       34
#define Y7_GPIO_NUM       39
#define Y6_GPIO_NUM       36
#define Y5_GPIO_NUM       21
#define Y4_GPIO_NUM       19
#define Y3_GPIO_NUM       18
#define Y2_GPIO_NUM        5
#define VSYNC_GPIO_NUM    25
#define HREF_GPIO_NUM     23
#define PCLK_GPIO_NUM     22
#define RX2_GPIO_NUM     16

const char* ssid     = "GOLBB_PRJ";
const char* password = "12345678";

AsyncWebServer server(80);
AsyncWebSocket wsCamera("/Camera");
AsyncWebSocket wsCarInput("/CarInput");
uint32_t cameraClientId = 0;
typedef struct {//放置下发数据的结构体
  uint8_t speed_DIR = 111;//速度方向
  uint32_t speed_LR = 0;//速度数值，采用2xx9格式发送
  uint8_t altitude = 0;//火炮俯仰角，采取角度制
  uint8_t turn = 0;//炮塔旋转 0停止，1顺时针，2逆时针
  uint8_t STAB = 0;//火控开关
  uint8_t addr = 0;//读写地址
  uint8_t change_flag = 0;//是否改变
  uint8_t light = 0;//灯光状态
  uint8_t Voltage = 80;
  uint8_t Received = 0;//是否有待发数据
}_wbccache;
_wbccache wbcache;
_wbccache* WBC = &wbcache;


void handleRoot(AsyncWebServerRequest *request) 
{
  request->send_P(200, "text/html", htmlHomePage);
}

void handleNotFound(AsyncWebServerRequest *request) 
{
    request->send(404, "text/plain", "File Not Found");
}



void onCarInputWebSocketEvent(AsyncWebSocket *server, 
                      AsyncWebSocketClient *client, 
                      AwsEventType type,
                      void *arg, 
                      uint8_t *data, 
                      size_t len) 
{                      
  switch (type) 
  {
    case WS_EVT_CONNECT:
      //Serial.printf("WebSocket client #%u connected from %s\n", client->id(), client->remoteIP().toString().c_str());
      break;
    case WS_EVT_DISCONNECT:
      //Serial.printf("WebSocket client #%u disconnected\n", client->id());
      ledcWrite(PWMLightChannel, 0);
      break;
    case WS_EVT_DATA:
      AwsFrameInfo *info;
      info = (AwsFrameInfo*)arg;
      if (info->final && info->index == 0 && info->len == len && info->opcode == WS_TEXT) 
      {
        std::string myData = "";
        myData.assign((char *)data, len);
        std::istringstream ss(myData);
        std::string key, value;
        std::getline(ss, key, ',');
        std::getline(ss, value, ',');
        int addrInt = atoi(key.c_str());
        int valueInt = atoi(value.c_str());
        //Serial.print("Data is :");Serial.print(addrInt);Serial.print("this time \n");//查看收到指令 
        switch (addrInt) {
          case 111:
            WBC->speed_LR = valueInt;
            WBC->addr=111;
            break;
          case 100:
            WBC->speed_LR = valueInt;
            WBC->addr=100;
            break;
          case 110:
            WBC->speed_LR = valueInt;
            WBC->addr=110;
            break;
          case 101:
            WBC->speed_LR = valueInt;
            WBC->addr=101;
            break;
          case 1:
            WBC->turn = valueInt;
            break;
          case 2:
            WBC->light = valueInt;
            break;
          case 3:
            WBC->STAB = valueInt;
            break;
        }
        WBC->Received = 1;
      }
      break;
    case WS_EVT_PONG:
    case WS_EVT_ERROR:
      break;
    default:
      break;  
  }
}

void onCameraWebSocketEvent(AsyncWebSocket *server, 
                      AsyncWebSocketClient *client, 
                      AwsEventType type,
                      void *arg, 
                      uint8_t *data, 
                      size_t len) 
{                      
  switch (type) 
  {
    case WS_EVT_CONNECT:
      //Serial.printf("WebSocket client #%u connected from %s\n", client->id(), client->remoteIP().toString().c_str());
      cameraClientId = client->id();
      break;
    case WS_EVT_DISCONNECT:
      //Serial.printf("WebSocket client #%u disconnected\n", client->id());
      cameraClientId = 0;
      break;
    case WS_EVT_DATA:
      break;
    case WS_EVT_PONG:
    case WS_EVT_ERROR:
      break;
    default:
      break;  
  }
}

void setupCamera()
{
  camera_config_t config;
  config.ledc_channel = LEDC_CHANNEL_0;
  config.ledc_timer = LEDC_TIMER_0;
  config.pin_d0 = Y2_GPIO_NUM;
  config.pin_d1 = Y3_GPIO_NUM;
  config.pin_d2 = Y4_GPIO_NUM;
  config.pin_d3 = Y5_GPIO_NUM;
  config.pin_d4 = Y6_GPIO_NUM;
  config.pin_d5 = Y7_GPIO_NUM;
  config.pin_d6 = Y8_GPIO_NUM;
  config.pin_d7 = Y9_GPIO_NUM;
  config.pin_xclk = XCLK_GPIO_NUM;
  config.pin_pclk = PCLK_GPIO_NUM;
  config.pin_vsync = VSYNC_GPIO_NUM;
  config.pin_href = HREF_GPIO_NUM;
  config.pin_sscb_sda = SIOD_GPIO_NUM;
  config.pin_sscb_scl = SIOC_GPIO_NUM;
  config.pin_pwdn = PWDN_GPIO_NUM;
  config.pin_reset = RESET_GPIO_NUM;
  config.xclk_freq_hz = 12000000;
  config.pixel_format = PIXFORMAT_JPEG;
  
  config.frame_size = FRAMESIZE_VGA;
  config.jpeg_quality = 12;
  config.fb_count = 5;

  // camera init
  esp_err_t err = esp_camera_init(&config);
  if (err != ESP_OK) 
  {
    //Serial.printf("Camera init failed with error 0x%x", err);
    return;
  }  

  if (psramFound())
  {
    heap_caps_malloc_extmem_enable(20000);  
    //Serial.printf("PSRAM initialized. malloc to take memory from psram above this size");    
  }  
}

void sendCameraPicture()
{
  if (cameraClientId == 0)
  {
    return;
  }
  unsigned long  startTime1 = millis();
  //capture a frame
  camera_fb_t * fb = esp_camera_fb_get();
  if (!fb) 
  {
      //Serial.println("Frame buffer could not be acquired");
      return;
  }

  unsigned long  startTime2 = millis();
  wsCamera.binary(cameraClientId, fb->buf, fb->len);
  esp_camera_fb_return(fb);
    
  //Wait for message to be delivered
  while (true)
  {
    AsyncWebSocketClient * clientPointer = wsCamera.client(cameraClientId);
    if (!clientPointer || !(clientPointer->queueIsFull()))
    {
      break;
    }
    delay(5);
  }
  unsigned long  startTime3 = millis();
}

void setup(void) 
{
  Serial.begin(115200);
  CARSlaveInit();// 配置I2C

  WiFi.softAP(ssid, password);
  IPAddress IP = WiFi.softAPIP();
  //Serial.print("AP IP address: ");
  //Serial.println(IP);

  server.on("/", HTTP_GET, handleRoot);
  server.onNotFound(handleNotFound);
      
  wsCamera.onEvent(onCameraWebSocketEvent);
  server.addHandler(&wsCamera);

  wsCarInput.onEvent(onCarInputWebSocketEvent);
  server.addHandler(&wsCarInput);

  server.begin();
  //Serial.println("HTTP server started");

  setupCamera();
  //Serial.print("Camera Ready! Use 'http://");
  //Serial.print(WiFi.localIP());
  //Serial.println("' to connect");
}

uint8_t value_80, value_08;
uint8_t counter = 0;
void loop() 
{
/*******************发送图片***********************/
  if(counter > 80){
    wsCamera.cleanupClients(); 
    wsCarInput.cleanupClients(); 
    sendCameraPicture(); 
    //Serial.printf("DATA1 %d\n", wbcache.turn);
    counter=0;
  }
  counter++;

/****************控制信号发送***********************/
  int verify = 0;
  value_80 = (uint8_t)(wbcache.speed_LR >> 8);
  value_08 = (uint8_t)(wbcache.speed_LR & 0xFF);//编码速度数据
  uint8_t arry[10] = {0xAA, CAR_SLAVE_ADDR, value_08, value_80, wbcache.addr, wbcache.turn, wbcache.light, wbcache.STAB, 0x00, 0xDD};//实际发送数据包
  for(int i =0;i<8;i++){verify += arry[i];}
  arry[8] = (uint8_t)(verify & 0xFF);
  WBC->Received = 0;
  printf("varify is %d\n", arry[8]);
  CAR_SendData(arry, 10);
/*************************************************/
  delay(1);
}
