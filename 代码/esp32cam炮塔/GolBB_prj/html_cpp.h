#include "esp_camera.h"
#include <Arduino.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <iostream>

const char* htmlHomePage PROGMEM = R"HTMLHOMEPAGE(
<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" 
        content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
        charset="UTF-8"
    >
    <style>
      .noselect {
        -webkit-touch-callout: none;
        /* iOS Safari */
        -webkit-user-select: none;
        /* Safari */
        -khtml-user-select: none;
        /* Konqueror HTML */
        -moz-user-select: none;
        /* Firefox */
        -ms-user-select: none;
        /* Internet Explorer/Edge */
        user-select: none;
        /* Non-prefixed version, currently
                                      supported by Chrome and Opera */
      }
      .slidecontainer {
        width: 100%;
      }
      .slider {
        -webkit-appearance: none;
        width: 100%;
        height: 15px;
        border-radius: 5px;
        background: #d3d3d3;
        outline: none;
        opacity: 0.7;
        -webkit-transition: .2s;
        transition: opacity .2s;
      }
      .slider:hover {
        opacity: 1;
      }
      .slider::-webkit-slider-thumb {
        -webkit-appearance: none;
        appearance: none;
        width: 25px;
        height: 25px;
        border-radius: 50%;
        background: red;
        cursor: pointer;
      }
      .slider::-moz-range-thumb {
        width: 25px;
        height: 25px;
        border-radius: 50%;
        background: red;
        cursor: pointer;
      }
      .button1 {
        background-color: #3CC9AC;
        /* Green */
        color: white;
        padding: 2% 30%;
        display: inline-block;
        border: 2px solid #625b57;
        border-radius: 30px;px;
        font-size: 16px;
        box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
      }
      .button1:hover {
        background-color: #51D67E
      }
      .button2 {
        background-color:#f66f6a;
        color:white;
        padding: 2% 30%;
        border: 2px solid #625b57;
        font-size: 16px;
        box-sizing: content-box;        
        border-radius: 30px;
        box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
      }
      .button2:hover {
        background-color: red
      }
      .button3 {
        background-color:dodgerblue;
        color:white;
        padding: 2% 30%;
        border: 2px solid #625b57;
        font-size: 16px;      
        border-radius: 30px;
        box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
      }
      .button3:hover {
        background-color: blue
      }
      .testswitch {
        position: relative;
        width: 90px;
        margin: 5% 7%;
       -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
      }
      .testswitch-checkbox {
        height: 50%;
        width: 50%;
        display: none;
      }
      .testswitch-label {
        display: block;
       overflow: hidden;
        cursor: pointer;
        border: 3px solid #999999;
        border-radius: 20px;
      }
      .testswitch-inner {
        display: block;
        width: 200%;
        margin-left: -100%;
        transition: margin 0.3s ease-in 0s;
      }
      .testswitch-inner::before,
      .testswitch-inner::after {
        display: block;
        float: right;
        width: 50%;
        height: 30px;
        padding: 0;
        line-height: 30px;
        font-size: 13px;
        color: white;
        font-family:
          Trebuchet, Arial, sans-serif;
        font-weight: bold;
        box-sizing: border-box;
      }
      .testswitch-inner::after {
        content: attr(data-on);
        padding-left: 10px;
        background-color: #00e500;
        color: #FFFFFF;
        text-align: left;
      }
      .testswitch-inner::before {
        content: attr(data-off);
        padding-right: 10px;
        background-color: #EEEEEE;
        color: #999999;
        text-align: right;
      }
      .testswitch-switch {
        position: absolute;
        display: block;
        width: 22px;
        height: 22px;
        margin: 4px;
        background: #FFFFFF;
        top: 0;
        bottom: 0;
        right: 56px;
        border: 2px solid #999999;
        border-radius: 20px;
        transition: all 0.3s ease-in 0s;
      }
      .testswitch-checkbox:checked+.testswitch-label .testswitch-inner {
        margin-left: 0;
      }
      .testswitch-checkbox:checked+.testswitch-label .testswitch-switch {
        right: 0px;
      }
      input[class="speed_ctrl"] {
        -webkit-appearance: slider-vertical;
        width: 100%;
        height: 100%;
        border-radius: 12px;
        box-shadow: 0px 0px 8px 0px #A4957D;
      }
      input[class="dir_ctrl"] {
        width: 70%;
        height: 30px;
        border-radius: 12px;
        box-shadow: 0px 0px 8px 0px #A4957D;
      }
      progress {
          width: 40px;
          height: 20px;
      }
    </style>
  </head>
  <body class="noselect" align="center" style="background-color:#c0c0c0">

    <!--h2 style="color: teal;text-align:center;">Wi-Fi Camera &#128663; Control</h2-->

    <table id="mainTable" style="width:100%;margin:auto;table-layout:fixed" CELLSPACING=10>
      <tr>
        <img id="cameraImage" src="" style="width:90%;height:230px">
        <input class="dir_ctrl" type="range" id="dir" max="99" min="-99" step="1"/>
        </tr>
      <tr>
        <th>
          <label id = "altitude">高低机</label>
          </th>
        <td>
          <button type="button" class="button1" id='rise' ontouchstart='tower_move("rise")'
            ontouchend='tower_move("stop")'>上升</button>
          <button type="button" class="button1" id='down' ontouchstart='tower_move("down")'
            ontouchend='tower_move("stop")'>下降</button>
          </td>
        <td>
          <button type="button" class="button3" id='reset' ontouchstart='tower_move("reset")'
            ontouchend='tower_move("stop")'>回正</button>
          <button type="button" class="button2" id='fire' ontouchstart='tower_move("fire")'
            ontouchend='tower_move("stop")'>开火</button>
          </td>
        </tr>
      <tr>
        <th>
          <label id = "battery">电量
            <progress value=20 max=100 id="Battery"></progress>
          </label>
          </th>
        <td>
          <div class="testswitch" id="LED_canvas">
            <input class="testswitch-checkbox" id="onoffswitch" type="checkbox"
              style="-webkit-appearance: checkbox;">
            <label class="testswitch-label" for="onoffswitch">
              <span class="testswitch-inner" data-on="D-ON" data-off="OFF"></span>
              <span class="testswitch-switch"></span>
              </label>
            </div>
          </td>
        <td>
          <div class="testswitch" id="STAB_canvas">
            <input class="testswitch-checkbox" id="onoffswitch2" type="checkbox">
            <label class="testswitch-label" for="onoffswitch2">
              <span class="testswitch-inner" data-on="STAB" data-off="MANUL"></span>
              <span class="testswitch-switch"></span>
              </label>
            </div>
          </td>
        </tr>
      </table>
    <table align="center" >
      <tr>
        <td>
          <input class="speed_ctrl" type="range" id="speed_L" max="99" min="-99" step="1"/></td>
        <td>
          <label id = "battery">TANK MODE</label></td>
        <td>
          <input class="speed_ctrl" type="range" id="speed_R" max="99" min="-99" step="1"/></td>
        </tr>
      </table>

    <script>
      window.addEventListener('load', load, false);
      var change_flag = [false, false, false, false]; //是否改变
      var switch_LED = document.getElementById("onoffswitch");
      var switch_STAB = document.getElementById("onoffswitch2");
      var slider_L = document.getElementById("speed_L");
      var slider_R = document.getElementById("speed_R");
      var slider_DIR = document.getElementById("dir");
      var light = '0';
      var dir = 0;
      var speed = [0,0];
      var Battery_Display = document.getElementById("Battery");
      var Battery = 80;//电池电压，不得小于7.4v 电压7.5v时发出警告
      
      //下发数据
      var self_adjust = false; //自动回正执行中，等待执行完成
      var speed_DIR = '111'; //速度方向 首位为占位符，1正0负
      var speed_LR = '10000'; //速度数值，首位为占位符，以aabb格式发送
      var altitude = '0'; //火炮俯仰角，采取角度制
      var turn = '0'; //炮塔旋转速度，发送时加100，解码时减100以表示负数
      var is_auto = '0'; //火控开关
      var LED_OF = false;
      var STAB_OF = false;
      var touched = [false,false];
      const sleep = (time) => {
        return new Promise(resolve => setTimeout(resolve, time));
      }
      
      function load() {
        function touch(event) {
          var event = event || window.event;
          var oInp = document.getElementById("inp");
        }
        
        function speed_reset(event){
          event.target.value=0;
        }
        document.getElementById('LED_canvas').addEventListener('touchstart',LED_touch,false);
        function LED_touch(event){
          LED_OF = !LED_OF;
          switch_LED.checked = !switch_LED.checked;
          change_flag[2] = true;
        }
        document.getElementById('STAB_canvas').addEventListener('touchstart',STAB_touch,false);
        function STAB_touch(event){
          STAB_OF = !STAB_OF;
          switch_STAB.checked=!switch_STAB.checked;
          change_flag[3] = true;
        }
        slider_L.addEventListener("touchend",speed_reset,false);
        slider_L.addEventListener("mouseup",speed_reset,false);
        slider_R.addEventListener("touchend",speed_reset,false);
        slider_R.addEventListener("mouseup",speed_reset,false);
        slider_DIR.addEventListener("touchend",speed_reset,false);
        slider_DIR.addEventListener("mouseup",speed_reset,false);
      }
      function tower_move(data) {
        if(!self_adjust){
          change_flag[1] = true;
        }
        switch (data) {
          case "stop":
            turn = '0';
            console.log('停止');
            break;
          case "clkwise":
            turn = '1';
            console.log('顺时针');
            break;
          case 'anti_clkwise':
            turn = '2';
            console.log('逆时针');
            break;
          case 'down':
            turn = '3';
            console.log('降低');
            break;
          case 'rise':
            turn = '4';
            console.log('升高');
            break;
          case 'reset':
            turn = '5';
            //self_adjust = true;
            console.log('回正');
            break;
          case 'fire':
            turn = '6';
            console.log('开火');
            break;
          default:
            turn = '0';
            console.log('停止');
            break;
        }
      }
      /*websocket协议部分*/
      var webSocketCameraUrl = "ws:\/\/" + window.location.hostname + "/Camera";
      var webSocketCarInputUrl = "ws:\/\/" + window.location.hostname + "/CarInput";
      var websocketCamera;
      var websocketCarInput;

      function initCameraWebSocket() {
        websocketCamera = new WebSocket(webSocketCameraUrl);
        websocketCamera.binaryType = 'blob';
        websocketCamera.onopen = function(event) {};
        websocketCamera.onclose = function(event) {
          setTimeout(initCameraWebSocket, 2000);
        };
        websocketCamera.onmessage = function(event) {
          var imageId = document.getElementById("cameraImage");
          imageId.src = URL.createObjectURL(event.data);
        };
        websocketCamera.onerror = function(event){
          console.log("图传发生错误！");
        };
      }

      function initCarInputWebSocket() {
        websocketCarInput = new WebSocket(webSocketCarInputUrl);
        websocketCarInput.onopen = function(event) {
          sending();
        };
        websocketCarInput.onclose = function(event) {
          setTimeout(initCarInputWebSocket, 2000);
        };
        websocketCarInput.onmessage = function(event) {
        };
        websocketCarInput.onerror = function(event){
          console.log("通信错误！");
        };
      }

      function initWebSocket() {
        initCameraWebSocket();
        sleep(100);
        initCarInputWebSocket();
      }

      function sendButtonInput(key, value) {
        var data = key + "," + value;
        websocketCarInput.send(data);
      }
      
      var battery_counter = 100;
      function Battery_check(){
        battery_counter-=1;
        if(battery_counter==0){
          sendButtonInput('201','0');//更新电池电压数据
          sleep(50);
          console.log(Battery);
          //if(Battery<=76){alert("电量极低，继续使用将损害电池！");}
          battery_counter = 100;
        }
        Battery_Display.value = (81-Battery)/(81-75)*100;
      }
      function sending(){
        /*下发数据*/
        contrast = speed_LR
        speed[0] = slider_L.value;
        speed[1] = slider_R.value;
        speed_DIR = "1" + (speed[0]>0? 1:0).toString() + (speed[1]>0? 1:0).toString();
        speed_LR = (10000 + Math.abs(speed[0]) * 100 + Math.abs(speed[1])).toString();
        if (contrast != speed_LR){change_flag[0] = true;}
        if (switch_STAB.checked) {
          if (turn != '5') {
            turn = '5';
            change_flag[1] = true;
          }
        }
        if (change_flag[0]) {
          sendButtonInput(speed_DIR,speed_LR);
          console.log(speed_LR);
          change_flag[0] = false;
        }
        if (change_flag[1]) {
          sendButtonInput('1',turn);
          console.log(turn);
          change_flag[1] = false;
        }
        if (change_flag[2]) {
          var sent = '0';
          if (LED_OF){sent = '1';}else{sent = '0';}
          sendButtonInput('2',sent);
          console.log(sent);
          change_flag[2] = false;
        }
        if (change_flag[3]) {
          var sent = '0';
          if (STAB_OF){sent = '1';}else{sent = '0';}
          sendButtonInput('3',sent);
          console.log(sent);
          change_flag[3] = false;
        }
        //Battery_check();
        sleep(20).then(() => {sending();});
      }
      window.onload = initWebSocket;
      
      document.getElementById("mainTable").addEventListener("touchend", function(event) {
        event.preventDefault()
      });
    </script>
  </body>
</html>
)HTMLHOMEPAGE";
