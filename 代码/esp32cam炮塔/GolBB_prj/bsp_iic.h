#ifndef _ESP_IIC_H_
#define _ESP_IIC_H_
 
#include <Arduino.h>
#include "driver/i2c.h"
 
#define ACK_VAL                 0x0        
#define NACK_VAL                0x1       
#define ACK_CHECK_EN            0x1     
#define ACK_CHECK_DIS           0x0                                                                                       

#define CAR_SCL_PIN             GPIO_NUM_14      
#define CAR_SDA_PIN             GPIO_NUM_15 
#define CAR_SLAVE_ADDR          0x61
#define I2C_SLAVE_NUM           I2C_NUM_0
#define AWAIT_BUF_TIME          10

#ifdef __cplusplus
extern "C" {
#endif

void CARSlaveInit(void);

void CAR_SendData(uint8_t* arry, int len);

#ifdef __cplusplus
}
#endif

#endif
