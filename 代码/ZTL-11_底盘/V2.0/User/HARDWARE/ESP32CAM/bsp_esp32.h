#ifndef __ESP32_H
#define __ESP32_H
#include "mpuiic.h"  
#include "param.h"

#define ESP32_ADDR				0x61		//定义器件地址
#define ESP32_READ				ESP32_ADDR
#define ESP32_WRITE				ESP32_ADDR + 1


typedef struct
{
	uint8_t TX_buf[128];		//输出数据缓存区
	uint8_t RX_buf[128];		//读取数据缓存区
}_I2Cbuf;

u8 ESP_Init(void); 								//初始化MPU6050
void ESP_Update(void);
uint8_t ESP_Receive(uint8_t *out);

u8 ESP_Write_Len(u8 addr,u8 reg,u8 len,u8 *buf);//IIC连续写
u8 ESP_Read_Len(u8 addr,u8 reg,u8 len,u8 *buf); //IIC连续读 
u8 ESP_Write_Byte(u8 reg,u8 data);				//IIC写一个字节
u8 ESP_Read_Byte(u8 reg);						//IIC读一个字节

#endif
