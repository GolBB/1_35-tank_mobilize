#ifndef __PARAM_H
#define __PARAM_H

#include "stm32f10x.h"

//主控参数
typedef struct
{
	uint8_t system_ready;		//系统开机
	uint8_t system_mode;		//系统控制模式
	int speed_L;		//左速度
	int speed_R;		//右速度
	uint8_t if_STAB;		//是否开启火控双稳
	int altitude;		//高低机角度
	int turn;				//方向机状态： 0停止 1顺时针 2逆时针
	int LED_mod;		//LED输出状态
	float Voltage;		//电池电压状态
	float VM;		//系统电池额定电压
	uint8_t SYS_STATE;		//系统错误码
	uint8_t ESP32_data[128];		//esp32数据
	uint8_t ESP32_data_len;		//esp32数据长度
}_CTRL;

extern _CTRL *CTR;


//核心常量
#define Servo_Speed 0.11		//设置方向机舵机极限速度（单位：秒/60度）
#define True 1
#define False 0
#define Pi 3.1415926

//系统错误码
#define SYS_ONLINE 0
#define SYS_ERROR 1
#define SYS_BLINK 2
#define SYS_LOWBATTERY 3
#define GYRO_OFFLINE 4

//双稳计算结构体
typedef struct
{
	float angle_tmp;		//当前火控指向角度
	float Pitch, Roll, Yaw;		//陀螺仪返回值
	int alt_pid; 		//高低机火控返回值
	int turn_pid;		//方向机火控返回值
}_STAB;

extern _STAB *STAB;

#endif
