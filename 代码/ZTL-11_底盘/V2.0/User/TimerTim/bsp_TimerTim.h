#ifndef __BSP_TIMERTIME_H
#define __BSP_TIMERTIME_H


#include "stm32f10x.h"


/**************通用定时器TIM参数定义，只限TIM2、3、4、5************/
// 当需要哪个定时器的时候，只需要把下面的宏定义改成1即可
#define TIMER_TIM2    1
#define TIMER_TIM3    0
#define TIMER_TIM4    0
#define TIMER_TIM5    0

#if  TIMER_TIM2
#define            TIMER_TIM                   TIM2
#define            TIMER_TIM_APBxClock_FUN     RCC_APB1PeriphClockCmd
#define            TIMER_TIM_CLK               RCC_APB1Periph_TIM2
#define            TIMER_TIM_Period            (4789-1)		//5毫秒计时
#define            TIMER_TIM_Prescaler         71
#define            TIMER_TIM_IRQ               TIM2_IRQn
#define            TIMER_TIM_IRQHandler        TIM2_IRQHandler

#elif  TIMER_TIM3
#define            TIMER_TIM                   TIM3
#define            TIMER_TIM_APBxClock_FUN     RCC_APB1PeriphClockCmd
#define            TIMER_TIM_CLK               RCC_APB1Periph_TIM3
#define            TIMER_TIM_Period            (1000-1)
#define            TIMER_TIM_Prescaler         71
#define            TIMER_TIM_IRQ               TIM3_IRQn
#define            TIMER_TIM_IRQHandler        TIM3_IRQHandler

#elif   TIMER_TIM4
#define            TIMER_TIM                   TIM4
#define            TIMER_TIM_APBxClock_FUN     RCC_APB1PeriphClockCmd
#define            TIMER_TIM_CLK               RCC_APB1Periph_TIM4
#define            TIMER_TIM_Period            (1000-1)
#define            TIMER_TIM_Prescaler         71
#define            TIMER_TIM_IRQ               TIM4_IRQn
#define            TIMER_TIM_IRQHandler        TIM4_IRQHandler

#elif   TIMER_TIM5
#define            TIMER_TIM                   TIM5
#define            TIMER_TIM_APBxClock_FUN     RCC_APB1PeriphClockCmd
#define            TIMER_TIM_CLK               RCC_APB1Periph_TIM5
#define            TIMER_TIM_Period            (1000-1)
#define            TIMER_TIM_Prescaler         71
#define            TIMER_TIM_IRQ               TIM5_IRQn
#define            TIMER_TIM_IRQHandler        TIM5_IRQHandler

#endif
/**************************函数声明********************************/

void TIMER_TIM_Init(void);
void TIM_Delay_subtract(void);
void TIM_Delay_5ms(u16 n);

#endif	/* __BSP_TIMERTIME_H */


