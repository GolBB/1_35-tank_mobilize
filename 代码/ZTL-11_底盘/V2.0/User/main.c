/**
  ******************************************************************************
  * @file    main.c
  * @author  GolBB
  * @version V2.0
  * @date    2023-xx-xx
  * @brief   stm32主板主程序
  ******************************************************************************
  * @attention
  *
  */ 
	
/*********************************includes*************************************/
#include "stm32f10x.h"
#include "delay.h"
#include "sys.h"
#include "bsp_led.h"
#include "bsp_usart.h"
#include "ADC.h"
#include "param.h"
#include "motor.h"
//#include "rtc.h"测试未通过
#include "bsp_TimerTim.h"
#include "delay.h"
#include "mpu6050.h"  
#include "inv_mpu.h"
#include "inv_mpu_dmp_motion_driver.h"
#include "bsp_esp32.h"



/*******************************系统参数设置************************************/
_CTRL CTRL;
_CTRL *CTR=&CTRL;
_STAB _STAB_;
_STAB *STAB = &_STAB_;

void params_init(void)
{
	CTR->VM = 0;		//指定电池电压最低值
	CTR->system_ready = 0;
	CTR->if_STAB = 0;
}


/**
  * @brief  主函数
  * @param  CTR  
  * @retval 无
  */
int main(void)
{
	params_init();
	/* 板载系统指示灯sys */
	LED_GPIO_Config();
	/* LED 端口初始化 */
	LED_OUT_Config();	
	/* 使能debug串口 */
	USART_Config();	
	/* 使能ADC */
	AD_Init();	
	/* 使能电机 */
	//GENERAL_TIM3_Init();	
	/* 初始化IIC总线 */
	MPU_IIC_Init();
	/* 初始ESP32CAM */
	ESP_Init();
	/* 使能MPU6050陀螺仪 */
	MPU_Init();	
	while(mpu_dmp_init()){
		delay_ms(20);
	}	
		/* 使能RTC系统外部时钟(未通过) */
	//if(RTC_Init()){CTR->system_ready = False;}else{CTR->system_ready = True;}	
	
	/* 使能系统时钟TIM2 */
	TIMER_TIM_Init();
	/* 系统自检通过 */
	SYS_INDICATOR(CTR->SYS_STATE);

	/************************************系统主循环************************************/
	while (!CTR->SYS_STATE)
	{
		TIM_Delay_5ms(20);		//主循环速度
		if(!CTR->if_STAB)
		{	/*手动控制*/
			printf("pitch：%f, roll：%f,Yaw：%f \n",STAB->Pitch,STAB->Roll,STAB->Yaw);
			printf("DATA0:%d DATA1: %d \n",CTRL.ESP32_data[1], CTRL.ESP32_data[3]<<8 | CTRL.ESP32_data[2]);
		}else
		{ /*开启双稳*/
			
		}
		
	/***********************************系统报错检测************************************/
		
		if(CTR->Voltage < CTR->VM){CTR->SYS_STATE = SYS_LOWBATTERY;}		//主板电压不足
	}
	
	
/**
  * @brief  系统异常退出处理
  * @param  CTR->SYS_STATE  
  * @retval 循环打印错误代码，并在板载指示灯上显示系统错误状态
  */
	while(1)
	{
		SYS_INDICATOR(CTR->SYS_STATE);
		printf("SYSTEM ERROR CODE :%d \n",CTR->SYS_STATE);
		TIM_Delay_5ms(200);
	}
}

/***********************************END OF FILE**************************************/
