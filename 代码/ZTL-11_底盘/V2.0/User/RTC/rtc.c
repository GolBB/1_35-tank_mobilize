#include "delay.h"
#include "rtc.h"

/*
 *==============================================================================
 *函数名称：RTC_Init
 *函数功能：初始化RTC
 *输入参数：无
 *返回值：0：成功；1：失败
 *备  注：无
 *==============================================================================
 */
u8 RTC_Init (void)
{
	u8 temp=0;   // 超时监控变量
	// 结构体定义
	NVIC_InitTypeDef NVIC_InitStructure;
	
	// 使能PWR和BKP外设时钟  
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR | RCC_APB1Periph_BKP, ENABLE); 
	PWR_BackupAccessCmd(ENABLE);   // 使能后备寄存器访问
	
	// 检测是否是第一次配置RTC
	// 配置时会想RTC寄存器写入0xA0A0，如果读出的数据不是0xA0A0，认为是第一次配置RTC
	if (BKP_ReadBackupRegister(BKP_DR1) != 0xA0A0)
	{ 			
		BKP_DeInit();   // 复位备份区域 	
		RCC_LSEConfig(RCC_LSE_ON);   // 设置外部低速晶振(LSE),使用外设低速晶振
		
		// 等待低速晶振就绪
		while (RCC_GetFlagStatus(RCC_FLAG_LSERDY) == RESET&&temp<250)
		{
			temp++;
			delay_ms(10);
		}
		// 初始化时钟失败,晶振有问题	
		if(temp>=250)
		{
			return 1;
		}			
		
		RCC_RTCCLKConfig(RCC_RTCCLKSource_LSE);   // 设置RTC时钟(RTCCLK),选择LSE作为RTC时钟    
		RCC_RTCCLKCmd(ENABLE);   // 使能RTC时钟  
		RTC_WaitForLastTask();   // 等待最近一次对RTC寄存器的写操作完成
		
		RTC_WaitForSynchro();   // 等待RTC寄存器同步
		RTC_ITConfig(RTC_IT_SEC, ENABLE);   // 使能RTC秒中断
		RTC_WaitForLastTask();   // 等待最近一次对RTC寄存器的写操作完成
		
		RTC_EnterConfigMode();   // 允许配置	
		RTC_SetPrescaler(32767);   // 设置RTC预分频的值
		RTC_WaitForLastTask();   // 等待最近一次对RTC寄存器的写操作完成
		
		RTC_ExitConfigMode();   // 退出配置模式  
		BKP_WriteBackupRegister(BKP_DR1, 0XA0A0);   // 向指定的后备寄存器中写入用户程序数据
	}
	// 系统继续计时
	else
	{
		RTC_WaitForSynchro();   // 等待最近一次对RTC寄存器的写操作完成
		RTC_ITConfig(RTC_IT_SEC, ENABLE);   // 使能RTC秒中断
		RTC_WaitForLastTask();   // 等待最近一次对RTC寄存器的写操作完成
	}
	
  // 配置RTC中断分组
	NVIC_InitStructure.NVIC_IRQChannel = RTC_IRQn;   // RTC全局中断
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;   // 先占优先级1位,从优先级3位
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;   // 先占优先级0位,从优先级4位
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;   // 使能该通道中断
	NVIC_Init(&NVIC_InitStructure);   // 根据NVIC_InitStruct中指定的参数初始化外设NVIC寄存器

	return 0;   // 配置成功
}
