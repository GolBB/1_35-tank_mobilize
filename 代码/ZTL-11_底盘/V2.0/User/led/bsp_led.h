#ifndef __LED_H
#define	__LED_H


#include "stm32f10x.h"


/* 定义LED连接的GPIO端口, 用户只需要修改下面的代码即可改变控制的LED引脚 */
// 板载指示灯
#define LED0_GPIO_PORT    	GPIOC			              /* GPIO端口 */
#define LED0_GPIO_CLK 	    RCC_APB2Periph_GPIOC		/* GPIO端口时钟 */
#define LED0_GPIO_PIN				GPIO_Pin_13		        /* 连接到SCL时钟线的GPIO */

// 外接LED（注意串联电阻）
#define LED_OUT_GPIO_PORT    	GPIOB
#define LED_OUT_GPIO_CLK 	    RCC_APB2Periph_GPIOB

#define LED1_GPIO_PIN				GPIO_Pin_3
#define LED2_GPIO_PIN				GPIO_Pin_4
#define LED3_GPIO_PIN				GPIO_Pin_5

#define ON  0
#define OFF 1

/* 使用标准的固件库控制IO*/
#define LED0(a)	if (a)	\
					GPIO_SetBits(LED0_GPIO_PORT,LED0_GPIO_PIN);\
					else		\
					GPIO_ResetBits(LED0_GPIO_PORT,LED0_GPIO_PIN)

/* 直接操作寄存器的方法控制IO */
#define	digitalHi(p,i)		 {p->BSRR=i;}	 //输出为高电平		
#define digitalLo(p,i)		 {p->BRR=i;}	 //输出低电平
#define digitalToggle(p,i) {p->ODR ^=i;} //输出反转状态


/* 定义控制IO的宏 */
#define LED0_TOGGLE		 digitalToggle(LED0_GPIO_PORT,LED0_GPIO_PIN)
#define LED0_OFF		   digitalHi(LED0_GPIO_PORT,LED0_GPIO_PIN)
#define LED0_ON			   digitalLo(LED0_GPIO_PORT,LED0_GPIO_PIN)

#define LED1_TOGGLE		 digitalToggle(LED1_GPIO_PORT,LED2_GPIO_PIN)
#define LED1_OFF		   digitalHi(LED1_GPIO_PORT,LED2_GPIO_PIN)
#define LED1_ON			   digitalLo(LED1_GPIO_PORT,LED2_GPIO_PIN)

#define LED2_TOGGLE		 digitalToggle(LED1_GPIO_PORT,LED3_GPIO_PIN)
#define LED2_OFF		   digitalHi(LED1_GPIO_PORT,LED3_GPIO_PIN)
#define LED2_ON			   digitalLo(LED1_GPIO_PORT,LED3_GPIO_PIN)

void LED_GPIO_Config(void);
void LED_OUT_Config(void);
void SYS_INDICATOR(int a);
void Delay(__IO uint32_t nCount);

#endif /* __LED_H */
