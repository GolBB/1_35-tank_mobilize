/**
  ******************************************************************************
  * @file    bsp_led.c
  * @author  GolBB
  * @version V2.0
  * @date    2023-xx-xx
  * @brief   led应用函数接口
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
  */
  
#include "bsp_led.h"   

 /**
  * @brief  初始化控制LED的IO
  * @param  无
  * @retval 无
  */
void LED_GPIO_Config(void)
{		
		/*定义一个GPIO_InitTypeDef类型的结构体*/
		GPIO_InitTypeDef GPIO_InitStructure;
		/*开启LED相关的GPIO外设时钟*/
		RCC_APB2PeriphClockCmd( LED0_GPIO_CLK , ENABLE);
		/*选择要控制的GPIO引脚*/
		GPIO_InitStructure.GPIO_Pin = LED0_GPIO_PIN;	
		/*设置引脚模式为通用推挽输出*/
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;   
		/*设置引脚速率为50MHz */   
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; 
		/*调用库函数，初始化GPIO*/
		GPIO_Init(LED0_GPIO_PORT, &GPIO_InitStructure);	
		/* 关闭所有led灯	*/
		GPIO_ResetBits(LED0_GPIO_PORT, LED0_GPIO_PIN);
}

void LED_OUT_Config(void)
{		
		/*定义一个GPIO_InitTypeDef类型的结构体*/
		GPIO_InitTypeDef GPIO_InitStructure;
		/*开启LED相关的GPIO外设时钟*/
		RCC_APB2PeriphClockCmd( LED_OUT_GPIO_CLK , ENABLE);
		/*选择要控制的GPIO引脚*/
		GPIO_InitStructure.GPIO_Pin = LED1_GPIO_PIN | LED2_GPIO_PIN | LED3_GPIO_PIN;
		/*设置引脚模式为通用推挽输出*/
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_OD;   
		/*设置引脚速率为50MHz */   
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; 
		/*调用库函数，初始化GPIO*/
		GPIO_Init(LED_OUT_GPIO_PORT, &GPIO_InitStructure);	
		/* 关闭所有led灯	*/
		GPIO_ResetBits(LED_OUT_GPIO_PORT, GPIO_InitStructure.GPIO_Pin);
}

#define SOFT_DELAY Delay(0x0FFFFF);
#define BLINK Delay(0x08FFFF);
void Delay(__IO uint32_t nCount)	 //简单的延时函数
{
	for(; nCount != 0; nCount--);
}

void SYS_INDICATOR(int a)
{
	int i;
	switch(a){
		case 0://系统上线
			LED0_OFF;
			SOFT_DELAY;
			SOFT_DELAY;
			for(i=0;i<2;i++){
				LED0_ON;
				BLINK;
				LED0_OFF;
				BLINK;
			}
			SOFT_DELAY;
			SOFT_DELAY;
			LED0_OFF;
			break;
		case 1://系统错误
			LED0_OFF;
			SOFT_DELAY;
			SOFT_DELAY;
			LED0_ON;
			SOFT_DELAY;
			SOFT_DELAY;
			LED0_OFF;
			SOFT_DELAY;
			for(i=0;i<2;i++){
				LED0_ON;
				BLINK;
				LED0_OFF;
				BLINK;
			}
			SOFT_DELAY;
			SOFT_DELAY;
			LED0_OFF;
			break;
		case 2://闪烁
			LED0_TOGGLE;
			break;
		case 3://电压不足
			LED0_OFF;
			SOFT_DELAY;
			SOFT_DELAY;
			for(i=0;i<3;i++){
				LED0_ON;
				BLINK;
				LED0_OFF;
				BLINK;
			}
			SOFT_DELAY;
			SOFT_DELAY;
			LED0_OFF;
			break;
	}
}
/*********************************************END OF FILE**********************/
