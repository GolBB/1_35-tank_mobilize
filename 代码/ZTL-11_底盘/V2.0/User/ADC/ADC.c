#include "ADC.h"                  // Device header
 
void AD_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	ADC_InitTypeDef ADC_InitStruct;
	//Open the clock
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA,ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1,ENABLE);
	
	RCC_ADCCLKConfig(RCC_PCLK2_Div6);//6??=72/6=12Mhz
	
	//GPIO
 	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
 	GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	ADC_RegularChannelConfig(ADC1,ADC_Channel_0, 1, ADC_SampleTime_55Cycles5);
	//ADC
	ADC_InitStruct.ADC_ContinuousConvMode=DISABLE;  
	ADC_InitStruct.ADC_DataAlign=ADC_DataAlign_Right;
	ADC_InitStruct.ADC_ExternalTrigConv=ADC_ExternalTrigConv_None;
	ADC_InitStruct.ADC_Mode=ADC_Mode_Independent;
	ADC_InitStruct.ADC_NbrOfChannel=1;
	ADC_InitStruct.ADC_ScanConvMode=DISABLE;
 
	ADC_Init(ADC1,&ADC_InitStruct);
	
	ADC_Cmd(ADC1,ENABLE);
	//ADC??
	ADC_ResetCalibration(ADC1);//reset
	while(ADC_GetResetCalibrationStatus(ADC1)==SET);//wait res
	ADC_StartCalibration(ADC1);//
	while(ADC_GetCalibrationStatus(ADC1)==SET);//??????
}
 
uint16_t AD_GetValue(void)
{
	ADC_SoftwareStartConvCmd(ADC1,ENABLE);
	while(ADC_GetFlagStatus(ADC1,ADC_FLAG_EOC)==0);//????
	
	return ADC_GetConversionValue(ADC1);
}

__IO float V[10] = {10,10,10,10,10,10,10,10,10,10};
__IO int Lo = 0; 
float Get_Voltage(void)
{
	int i= 0;
	float sum = 0.0;
	if(Lo == 10)Lo = 0;
	V[Lo] = AD_GetValue() * 4 * 3.3 / 4096+0.3;
	Lo++;
	for(i = 0;i<10;i++){
		sum+=V[i];
	}
	sum = sum*0.1;
	return sum;
}
